#!/bin/bash

aws cloudformation package \
    --template-file ddb-template.yaml \
    --s3-bucket staging-bucket-us-east-1-joeyd \
    --output-template-file ddb-output-template.yaml
    