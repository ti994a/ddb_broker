from __future__ import print_function

import os
import boto3
import json

print('Loading function')

def handler(event, context):
    '''
    Provide an event that contains the following keys:

      - operation: one of the operations in the operations dict below
      - payload: a parameter to pass to the operation being performed

        sample request payload:
        {
            "operation": "create",
            "payload": {
                "Item": {
                    "id": "1",
                    "name": "Bob"
                }
            }
        }
        See: https://docs.aws.amazon.com/lambda/latest/dg/with-on-demand-https-example.html
    '''
    # print("Received event: " + json.dumps(event, indent=2))

    postData = json.loads(event['body'])
    operation = postData.get('operation')
    payload = postData.get('payload')
    tableName = os.environ['TABLE_NAME']

    print ("operation: " + operation)
    print ("tableName: " + tableName)
    print("payload: " + json.dumps(payload, indent=2))
    
    ddb_table = boto3.resource('dynamodb').Table(tableName)

    operations = {
        'create': lambda x: ddb_table.put_item(**x),
        'read': lambda x: ddb_table.get_item(**x),
        'update': lambda x: ddb_table.update_item(**x),
        'delete': lambda x: ddb_table.delete_item(**x),
        'list': lambda x: ddb_table.scan(**x),
        'echo': lambda x: x,
        'ping': lambda x: 'pong'
    }

    if operation in operations:
        returnValue = operations[operation](payload)
        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json'},
            'body': json.dumps(returnValue)
        }
    else:
        raise ValueError('Unrecognized operation "{}"'.format(operation))