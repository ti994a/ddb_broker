#!/bin/bash

aws cloudformation deploy \
    --template-file ddb-output-template.yaml \
    --stack-name ddb-broker \
    --capabilities CAPABILITY_IAM

